// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".user_fields").hide();
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().append(content.replace(regexp, new_id));
}
