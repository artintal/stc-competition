json.array!(@teams) do |team|
  json.extract! team, :id, :name, :project_name, :project_description
  json.url team_url(team, format: :json)
end
