class User < ActiveRecord::Base
  belongs_to :team

  enum role: [:user, :admin]
  after_initialize :set_default_role, :if => :new_record?

  validates :full_name, presence: true
  EMAIL_REGEX = /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  validates :email, presence: true, uniqueness: true
  validates_format_of :email, with: EMAIL_REGEX

  def set_default_role
    self.role = :user
  end
end
