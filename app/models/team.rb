class Team < ActiveRecord::Base
  has_many :users
  validates :name, presence: true
  validates :project_name, presence: true
  validates :project_description, presence: true
  has_secure_password
  validates :password, length: {minimum: 4}
  accepts_nested_attributes_for :users, allow_destroy: true, reject_if: proc {|a| a["full_name"].blank? and a["email"].blank?}
end
