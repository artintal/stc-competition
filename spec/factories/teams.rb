# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :team do
    name "MyString"
    project_name "MyString"
    project_description "MyText"
  end
end
