Rails.application.routes.draw do
  resources :teams do
    member do
      delete 'destroy_user/:user_id', action: :destroy_user, as: :remove_user_from
    end
  end
  get '/home', to: 'teams#home'
  get '/comingsoon', to: 'teams#coming_soon'
  root to: 'teams#home'
end
